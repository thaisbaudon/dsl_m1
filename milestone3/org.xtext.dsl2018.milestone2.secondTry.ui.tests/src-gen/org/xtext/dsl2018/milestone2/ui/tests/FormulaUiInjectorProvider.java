/*
 * generated by Xtext 2.15.0
 */
package org.xtext.dsl2018.milestone2.ui.tests;

import com.google.inject.Injector;
import org.eclipse.xtext.testing.IInjectorProvider;
import org.xtext.dsl2018.milestone2.secondTry.ui.internal.SecondTryActivator;

public class FormulaUiInjectorProvider implements IInjectorProvider {

	@Override
	public Injector getInjector() {
		return SecondTryActivator.getInstance().getInjector("org.xtext.dsl2018.milestone2.Formula");
	}

}
