package org.xtext.dsl2018.milestone2.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.dsl2018.milestone2.services.FormulaGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFormulaParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'TRUE'", "'FALSE'", "'~'", "'('", "')'", "'/\\\\'", "'\\\\/'", "'->'", "'<->'", "'|'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalFormulaParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFormulaParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFormulaParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFormula.g"; }



     	private FormulaGrammarAccess grammarAccess;

        public InternalFormulaParser(TokenStream input, FormulaGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Formula";
       	}

       	@Override
       	protected FormulaGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleFormula"
    // InternalFormula.g:64:1: entryRuleFormula returns [EObject current=null] : iv_ruleFormula= ruleFormula EOF ;
    public final EObject entryRuleFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormula = null;


        try {
            // InternalFormula.g:64:48: (iv_ruleFormula= ruleFormula EOF )
            // InternalFormula.g:65:2: iv_ruleFormula= ruleFormula EOF
            {
             newCompositeNode(grammarAccess.getFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFormula=ruleFormula();

            state._fsp--;

             current =iv_ruleFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // InternalFormula.g:71:1: ruleFormula returns [EObject current=null] : (this_Constant_0= ruleConstant | this_Variable_1= ruleVariable | this_Neg_2= ruleNeg | this_Binop_3= ruleBinop ) ;
    public final EObject ruleFormula() throws RecognitionException {
        EObject current = null;

        EObject this_Constant_0 = null;

        EObject this_Variable_1 = null;

        EObject this_Neg_2 = null;

        EObject this_Binop_3 = null;



        	enterRule();

        try {
            // InternalFormula.g:77:2: ( (this_Constant_0= ruleConstant | this_Variable_1= ruleVariable | this_Neg_2= ruleNeg | this_Binop_3= ruleBinop ) )
            // InternalFormula.g:78:2: (this_Constant_0= ruleConstant | this_Variable_1= ruleVariable | this_Neg_2= ruleNeg | this_Binop_3= ruleBinop )
            {
            // InternalFormula.g:78:2: (this_Constant_0= ruleConstant | this_Variable_1= ruleVariable | this_Neg_2= ruleNeg | this_Binop_3= ruleBinop )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 11:
            case 12:
                {
                alt1=1;
                }
                break;
            case RULE_ID:
                {
                alt1=2;
                }
                break;
            case 13:
                {
                alt1=3;
                }
                break;
            case 14:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalFormula.g:79:3: this_Constant_0= ruleConstant
                    {

                    			newCompositeNode(grammarAccess.getFormulaAccess().getConstantParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Constant_0=ruleConstant();

                    state._fsp--;


                    			current = this_Constant_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalFormula.g:88:3: this_Variable_1= ruleVariable
                    {

                    			newCompositeNode(grammarAccess.getFormulaAccess().getVariableParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Variable_1=ruleVariable();

                    state._fsp--;


                    			current = this_Variable_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalFormula.g:97:3: this_Neg_2= ruleNeg
                    {

                    			newCompositeNode(grammarAccess.getFormulaAccess().getNegParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Neg_2=ruleNeg();

                    state._fsp--;


                    			current = this_Neg_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalFormula.g:106:3: this_Binop_3= ruleBinop
                    {

                    			newCompositeNode(grammarAccess.getFormulaAccess().getBinopParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Binop_3=ruleBinop();

                    state._fsp--;


                    			current = this_Binop_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleConstant"
    // InternalFormula.g:118:1: entryRuleConstant returns [EObject current=null] : iv_ruleConstant= ruleConstant EOF ;
    public final EObject entryRuleConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstant = null;


        try {
            // InternalFormula.g:118:49: (iv_ruleConstant= ruleConstant EOF )
            // InternalFormula.g:119:2: iv_ruleConstant= ruleConstant EOF
            {
             newCompositeNode(grammarAccess.getConstantRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstant=ruleConstant();

            state._fsp--;

             current =iv_ruleConstant; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // InternalFormula.g:125:1: ruleConstant returns [EObject current=null] : ( (otherlv_0= 'TRUE' () ) | (otherlv_2= 'FALSE' () ) ) ;
    public final EObject ruleConstant() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalFormula.g:131:2: ( ( (otherlv_0= 'TRUE' () ) | (otherlv_2= 'FALSE' () ) ) )
            // InternalFormula.g:132:2: ( (otherlv_0= 'TRUE' () ) | (otherlv_2= 'FALSE' () ) )
            {
            // InternalFormula.g:132:2: ( (otherlv_0= 'TRUE' () ) | (otherlv_2= 'FALSE' () ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalFormula.g:133:3: (otherlv_0= 'TRUE' () )
                    {
                    // InternalFormula.g:133:3: (otherlv_0= 'TRUE' () )
                    // InternalFormula.g:134:4: otherlv_0= 'TRUE' ()
                    {
                    otherlv_0=(Token)match(input,11,FOLLOW_2); 

                    				newLeafNode(otherlv_0, grammarAccess.getConstantAccess().getTRUEKeyword_0_0());
                    			
                    // InternalFormula.g:138:4: ()
                    // InternalFormula.g:139:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getConstantAccess().getConstantAction_0_1(),
                    						current);
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:147:3: (otherlv_2= 'FALSE' () )
                    {
                    // InternalFormula.g:147:3: (otherlv_2= 'FALSE' () )
                    // InternalFormula.g:148:4: otherlv_2= 'FALSE' ()
                    {
                    otherlv_2=(Token)match(input,12,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getConstantAccess().getFALSEKeyword_1_0());
                    			
                    // InternalFormula.g:152:4: ()
                    // InternalFormula.g:153:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getConstantAccess().getConstantAction_1_1(),
                    						current);
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleVariable"
    // InternalFormula.g:164:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalFormula.g:164:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalFormula.g:165:2: iv_ruleVariable= ruleVariable EOF
            {
             newCompositeNode(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;

             current =iv_ruleVariable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalFormula.g:171:1: ruleVariable returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalFormula.g:177:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalFormula.g:178:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalFormula.g:178:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalFormula.g:179:3: (lv_name_0_0= RULE_ID )
            {
            // InternalFormula.g:179:3: (lv_name_0_0= RULE_ID )
            // InternalFormula.g:180:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getVariableRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleNeg"
    // InternalFormula.g:199:1: entryRuleNeg returns [EObject current=null] : iv_ruleNeg= ruleNeg EOF ;
    public final EObject entryRuleNeg() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNeg = null;


        try {
            // InternalFormula.g:199:44: (iv_ruleNeg= ruleNeg EOF )
            // InternalFormula.g:200:2: iv_ruleNeg= ruleNeg EOF
            {
             newCompositeNode(grammarAccess.getNegRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNeg=ruleNeg();

            state._fsp--;

             current =iv_ruleNeg; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNeg"


    // $ANTLR start "ruleNeg"
    // InternalFormula.g:206:1: ruleNeg returns [EObject current=null] : (otherlv_0= '~' ( (lv_sub_1_0= ruleFormula ) ) ) ;
    public final EObject ruleNeg() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_sub_1_0 = null;



        	enterRule();

        try {
            // InternalFormula.g:212:2: ( (otherlv_0= '~' ( (lv_sub_1_0= ruleFormula ) ) ) )
            // InternalFormula.g:213:2: (otherlv_0= '~' ( (lv_sub_1_0= ruleFormula ) ) )
            {
            // InternalFormula.g:213:2: (otherlv_0= '~' ( (lv_sub_1_0= ruleFormula ) ) )
            // InternalFormula.g:214:3: otherlv_0= '~' ( (lv_sub_1_0= ruleFormula ) )
            {
            otherlv_0=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getNegAccess().getTildeKeyword_0());
            		
            // InternalFormula.g:218:3: ( (lv_sub_1_0= ruleFormula ) )
            // InternalFormula.g:219:4: (lv_sub_1_0= ruleFormula )
            {
            // InternalFormula.g:219:4: (lv_sub_1_0= ruleFormula )
            // InternalFormula.g:220:5: lv_sub_1_0= ruleFormula
            {

            					newCompositeNode(grammarAccess.getNegAccess().getSubFormulaParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_sub_1_0=ruleFormula();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNegRule());
            					}
            					set(
            						current,
            						"sub",
            						lv_sub_1_0,
            						"org.xtext.dsl2018.milestone2.Formula.Formula");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNeg"


    // $ANTLR start "entryRuleBinop"
    // InternalFormula.g:241:1: entryRuleBinop returns [EObject current=null] : iv_ruleBinop= ruleBinop EOF ;
    public final EObject entryRuleBinop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinop = null;


        try {
            // InternalFormula.g:241:46: (iv_ruleBinop= ruleBinop EOF )
            // InternalFormula.g:242:2: iv_ruleBinop= ruleBinop EOF
            {
             newCompositeNode(grammarAccess.getBinopRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBinop=ruleBinop();

            state._fsp--;

             current =iv_ruleBinop; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinop"


    // $ANTLR start "ruleBinop"
    // InternalFormula.g:248:1: ruleBinop returns [EObject current=null] : (otherlv_0= '(' this_Formula_1= ruleFormula () ( (lv_op_3_0= ruleOp ) ) ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ')' ) ;
    public final EObject ruleBinop() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_5=null;
        EObject this_Formula_1 = null;

        AntlrDatatypeRuleToken lv_op_3_0 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalFormula.g:254:2: ( (otherlv_0= '(' this_Formula_1= ruleFormula () ( (lv_op_3_0= ruleOp ) ) ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ')' ) )
            // InternalFormula.g:255:2: (otherlv_0= '(' this_Formula_1= ruleFormula () ( (lv_op_3_0= ruleOp ) ) ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ')' )
            {
            // InternalFormula.g:255:2: (otherlv_0= '(' this_Formula_1= ruleFormula () ( (lv_op_3_0= ruleOp ) ) ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ')' )
            // InternalFormula.g:256:3: otherlv_0= '(' this_Formula_1= ruleFormula () ( (lv_op_3_0= ruleOp ) ) ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getBinopAccess().getLeftParenthesisKeyword_0());
            		

            			newCompositeNode(grammarAccess.getBinopAccess().getFormulaParserRuleCall_1());
            		
            pushFollow(FOLLOW_4);
            this_Formula_1=ruleFormula();

            state._fsp--;


            			current = this_Formula_1;
            			afterParserOrEnumRuleCall();
            		
            // InternalFormula.g:268:3: ()
            // InternalFormula.g:269:4: 
            {

            				current = forceCreateModelElementAndSet(
            					grammarAccess.getBinopAccess().getBinopLeftAction_2(),
            					current);
            			

            }

            // InternalFormula.g:275:3: ( (lv_op_3_0= ruleOp ) )
            // InternalFormula.g:276:4: (lv_op_3_0= ruleOp )
            {
            // InternalFormula.g:276:4: (lv_op_3_0= ruleOp )
            // InternalFormula.g:277:5: lv_op_3_0= ruleOp
            {

            					newCompositeNode(grammarAccess.getBinopAccess().getOpOpParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_3);
            lv_op_3_0=ruleOp();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBinopRule());
            					}
            					set(
            						current,
            						"op",
            						lv_op_3_0,
            						"org.xtext.dsl2018.milestone2.Formula.Op");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFormula.g:294:3: ( (lv_right_4_0= ruleFormula ) )
            // InternalFormula.g:295:4: (lv_right_4_0= ruleFormula )
            {
            // InternalFormula.g:295:4: (lv_right_4_0= ruleFormula )
            // InternalFormula.g:296:5: lv_right_4_0= ruleFormula
            {

            					newCompositeNode(grammarAccess.getBinopAccess().getRightFormulaParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_5);
            lv_right_4_0=ruleFormula();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBinopRule());
            					}
            					set(
            						current,
            						"right",
            						lv_right_4_0,
            						"org.xtext.dsl2018.milestone2.Formula.Formula");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getBinopAccess().getRightParenthesisKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinop"


    // $ANTLR start "entryRuleOp"
    // InternalFormula.g:321:1: entryRuleOp returns [String current=null] : iv_ruleOp= ruleOp EOF ;
    public final String entryRuleOp() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOp = null;


        try {
            // InternalFormula.g:321:42: (iv_ruleOp= ruleOp EOF )
            // InternalFormula.g:322:2: iv_ruleOp= ruleOp EOF
            {
             newCompositeNode(grammarAccess.getOpRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOp=ruleOp();

            state._fsp--;

             current =iv_ruleOp.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOp"


    // $ANTLR start "ruleOp"
    // InternalFormula.g:328:1: ruleOp returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '/\\\\' | kw= '\\\\/' | kw= '->' | kw= '<->' | kw= '|' ) ;
    public final AntlrDatatypeRuleToken ruleOp() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalFormula.g:334:2: ( (kw= '/\\\\' | kw= '\\\\/' | kw= '->' | kw= '<->' | kw= '|' ) )
            // InternalFormula.g:335:2: (kw= '/\\\\' | kw= '\\\\/' | kw= '->' | kw= '<->' | kw= '|' )
            {
            // InternalFormula.g:335:2: (kw= '/\\\\' | kw= '\\\\/' | kw= '->' | kw= '<->' | kw= '|' )
            int alt3=5;
            switch ( input.LA(1) ) {
            case 16:
                {
                alt3=1;
                }
                break;
            case 17:
                {
                alt3=2;
                }
                break;
            case 18:
                {
                alt3=3;
                }
                break;
            case 19:
                {
                alt3=4;
                }
                break;
            case 20:
                {
                alt3=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalFormula.g:336:3: kw= '/\\\\'
                    {
                    kw=(Token)match(input,16,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpAccess().getSolidusReverseSolidusKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalFormula.g:342:3: kw= '\\\\/'
                    {
                    kw=(Token)match(input,17,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpAccess().getReverseSolidusSolidusKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalFormula.g:348:3: kw= '->'
                    {
                    kw=(Token)match(input,18,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpAccess().getHyphenMinusGreaterThanSignKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalFormula.g:354:3: kw= '<->'
                    {
                    kw=(Token)match(input,19,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalFormula.g:360:3: kw= '|'
                    {
                    kw=(Token)match(input,20,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpAccess().getVerticalLineKeyword_4());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOp"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000007810L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x00000000001F0000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000008000L});

}