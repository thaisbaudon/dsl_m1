package org.xtext.dsl2018.milestone2.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.dsl2018.milestone2.services.FormulaGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFormulaParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'/\\\\'", "'\\\\/'", "'->'", "'<->'", "'|'", "'TRUE'", "'FALSE'", "'~'", "'('", "')'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalFormulaParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFormulaParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFormulaParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFormula.g"; }


    	private FormulaGrammarAccess grammarAccess;

    	public void setGrammarAccess(FormulaGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleFormula"
    // InternalFormula.g:53:1: entryRuleFormula : ruleFormula EOF ;
    public final void entryRuleFormula() throws RecognitionException {
        try {
            // InternalFormula.g:54:1: ( ruleFormula EOF )
            // InternalFormula.g:55:1: ruleFormula EOF
            {
             before(grammarAccess.getFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // InternalFormula.g:62:1: ruleFormula : ( ( rule__Formula__Alternatives ) ) ;
    public final void ruleFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:66:2: ( ( ( rule__Formula__Alternatives ) ) )
            // InternalFormula.g:67:2: ( ( rule__Formula__Alternatives ) )
            {
            // InternalFormula.g:67:2: ( ( rule__Formula__Alternatives ) )
            // InternalFormula.g:68:3: ( rule__Formula__Alternatives )
            {
             before(grammarAccess.getFormulaAccess().getAlternatives()); 
            // InternalFormula.g:69:3: ( rule__Formula__Alternatives )
            // InternalFormula.g:69:4: rule__Formula__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Formula__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFormulaAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleConstant"
    // InternalFormula.g:78:1: entryRuleConstant : ruleConstant EOF ;
    public final void entryRuleConstant() throws RecognitionException {
        try {
            // InternalFormula.g:79:1: ( ruleConstant EOF )
            // InternalFormula.g:80:1: ruleConstant EOF
            {
             before(grammarAccess.getConstantRule()); 
            pushFollow(FOLLOW_1);
            ruleConstant();

            state._fsp--;

             after(grammarAccess.getConstantRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // InternalFormula.g:87:1: ruleConstant : ( ( rule__Constant__Alternatives ) ) ;
    public final void ruleConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:91:2: ( ( ( rule__Constant__Alternatives ) ) )
            // InternalFormula.g:92:2: ( ( rule__Constant__Alternatives ) )
            {
            // InternalFormula.g:92:2: ( ( rule__Constant__Alternatives ) )
            // InternalFormula.g:93:3: ( rule__Constant__Alternatives )
            {
             before(grammarAccess.getConstantAccess().getAlternatives()); 
            // InternalFormula.g:94:3: ( rule__Constant__Alternatives )
            // InternalFormula.g:94:4: rule__Constant__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleVariable"
    // InternalFormula.g:103:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // InternalFormula.g:104:1: ( ruleVariable EOF )
            // InternalFormula.g:105:1: ruleVariable EOF
            {
             before(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalFormula.g:112:1: ruleVariable : ( ( rule__Variable__NameAssignment ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:116:2: ( ( ( rule__Variable__NameAssignment ) ) )
            // InternalFormula.g:117:2: ( ( rule__Variable__NameAssignment ) )
            {
            // InternalFormula.g:117:2: ( ( rule__Variable__NameAssignment ) )
            // InternalFormula.g:118:3: ( rule__Variable__NameAssignment )
            {
             before(grammarAccess.getVariableAccess().getNameAssignment()); 
            // InternalFormula.g:119:3: ( rule__Variable__NameAssignment )
            // InternalFormula.g:119:4: rule__Variable__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Variable__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleNeg"
    // InternalFormula.g:128:1: entryRuleNeg : ruleNeg EOF ;
    public final void entryRuleNeg() throws RecognitionException {
        try {
            // InternalFormula.g:129:1: ( ruleNeg EOF )
            // InternalFormula.g:130:1: ruleNeg EOF
            {
             before(grammarAccess.getNegRule()); 
            pushFollow(FOLLOW_1);
            ruleNeg();

            state._fsp--;

             after(grammarAccess.getNegRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNeg"


    // $ANTLR start "ruleNeg"
    // InternalFormula.g:137:1: ruleNeg : ( ( rule__Neg__Group__0 ) ) ;
    public final void ruleNeg() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:141:2: ( ( ( rule__Neg__Group__0 ) ) )
            // InternalFormula.g:142:2: ( ( rule__Neg__Group__0 ) )
            {
            // InternalFormula.g:142:2: ( ( rule__Neg__Group__0 ) )
            // InternalFormula.g:143:3: ( rule__Neg__Group__0 )
            {
             before(grammarAccess.getNegAccess().getGroup()); 
            // InternalFormula.g:144:3: ( rule__Neg__Group__0 )
            // InternalFormula.g:144:4: rule__Neg__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Neg__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNegAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNeg"


    // $ANTLR start "entryRuleBinop"
    // InternalFormula.g:153:1: entryRuleBinop : ruleBinop EOF ;
    public final void entryRuleBinop() throws RecognitionException {
        try {
            // InternalFormula.g:154:1: ( ruleBinop EOF )
            // InternalFormula.g:155:1: ruleBinop EOF
            {
             before(grammarAccess.getBinopRule()); 
            pushFollow(FOLLOW_1);
            ruleBinop();

            state._fsp--;

             after(grammarAccess.getBinopRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinop"


    // $ANTLR start "ruleBinop"
    // InternalFormula.g:162:1: ruleBinop : ( ( rule__Binop__Group__0 ) ) ;
    public final void ruleBinop() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:166:2: ( ( ( rule__Binop__Group__0 ) ) )
            // InternalFormula.g:167:2: ( ( rule__Binop__Group__0 ) )
            {
            // InternalFormula.g:167:2: ( ( rule__Binop__Group__0 ) )
            // InternalFormula.g:168:3: ( rule__Binop__Group__0 )
            {
             before(grammarAccess.getBinopAccess().getGroup()); 
            // InternalFormula.g:169:3: ( rule__Binop__Group__0 )
            // InternalFormula.g:169:4: rule__Binop__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Binop__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBinopAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinop"


    // $ANTLR start "entryRuleOp"
    // InternalFormula.g:178:1: entryRuleOp : ruleOp EOF ;
    public final void entryRuleOp() throws RecognitionException {
        try {
            // InternalFormula.g:179:1: ( ruleOp EOF )
            // InternalFormula.g:180:1: ruleOp EOF
            {
             before(grammarAccess.getOpRule()); 
            pushFollow(FOLLOW_1);
            ruleOp();

            state._fsp--;

             after(grammarAccess.getOpRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOp"


    // $ANTLR start "ruleOp"
    // InternalFormula.g:187:1: ruleOp : ( ( rule__Op__Alternatives ) ) ;
    public final void ruleOp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:191:2: ( ( ( rule__Op__Alternatives ) ) )
            // InternalFormula.g:192:2: ( ( rule__Op__Alternatives ) )
            {
            // InternalFormula.g:192:2: ( ( rule__Op__Alternatives ) )
            // InternalFormula.g:193:3: ( rule__Op__Alternatives )
            {
             before(grammarAccess.getOpAccess().getAlternatives()); 
            // InternalFormula.g:194:3: ( rule__Op__Alternatives )
            // InternalFormula.g:194:4: rule__Op__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Op__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOpAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOp"


    // $ANTLR start "rule__Formula__Alternatives"
    // InternalFormula.g:202:1: rule__Formula__Alternatives : ( ( ruleConstant ) | ( ruleVariable ) | ( ruleNeg ) | ( ruleBinop ) );
    public final void rule__Formula__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:206:1: ( ( ruleConstant ) | ( ruleVariable ) | ( ruleNeg ) | ( ruleBinop ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 16:
            case 17:
                {
                alt1=1;
                }
                break;
            case RULE_ID:
                {
                alt1=2;
                }
                break;
            case 18:
                {
                alt1=3;
                }
                break;
            case 19:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalFormula.g:207:2: ( ruleConstant )
                    {
                    // InternalFormula.g:207:2: ( ruleConstant )
                    // InternalFormula.g:208:3: ruleConstant
                    {
                     before(grammarAccess.getFormulaAccess().getConstantParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleConstant();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getConstantParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:213:2: ( ruleVariable )
                    {
                    // InternalFormula.g:213:2: ( ruleVariable )
                    // InternalFormula.g:214:3: ruleVariable
                    {
                     before(grammarAccess.getFormulaAccess().getVariableParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleVariable();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getVariableParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFormula.g:219:2: ( ruleNeg )
                    {
                    // InternalFormula.g:219:2: ( ruleNeg )
                    // InternalFormula.g:220:3: ruleNeg
                    {
                     before(grammarAccess.getFormulaAccess().getNegParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleNeg();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getNegParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFormula.g:225:2: ( ruleBinop )
                    {
                    // InternalFormula.g:225:2: ( ruleBinop )
                    // InternalFormula.g:226:3: ruleBinop
                    {
                     before(grammarAccess.getFormulaAccess().getBinopParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleBinop();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getBinopParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Alternatives"


    // $ANTLR start "rule__Constant__Alternatives"
    // InternalFormula.g:235:1: rule__Constant__Alternatives : ( ( ( rule__Constant__Group_0__0 ) ) | ( ( rule__Constant__Group_1__0 ) ) );
    public final void rule__Constant__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:239:1: ( ( ( rule__Constant__Group_0__0 ) ) | ( ( rule__Constant__Group_1__0 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==16) ) {
                alt2=1;
            }
            else if ( (LA2_0==17) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalFormula.g:240:2: ( ( rule__Constant__Group_0__0 ) )
                    {
                    // InternalFormula.g:240:2: ( ( rule__Constant__Group_0__0 ) )
                    // InternalFormula.g:241:3: ( rule__Constant__Group_0__0 )
                    {
                     before(grammarAccess.getConstantAccess().getGroup_0()); 
                    // InternalFormula.g:242:3: ( rule__Constant__Group_0__0 )
                    // InternalFormula.g:242:4: rule__Constant__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Constant__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstantAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:246:2: ( ( rule__Constant__Group_1__0 ) )
                    {
                    // InternalFormula.g:246:2: ( ( rule__Constant__Group_1__0 ) )
                    // InternalFormula.g:247:3: ( rule__Constant__Group_1__0 )
                    {
                     before(grammarAccess.getConstantAccess().getGroup_1()); 
                    // InternalFormula.g:248:3: ( rule__Constant__Group_1__0 )
                    // InternalFormula.g:248:4: rule__Constant__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Constant__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstantAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Alternatives"


    // $ANTLR start "rule__Op__Alternatives"
    // InternalFormula.g:256:1: rule__Op__Alternatives : ( ( '/\\\\' ) | ( '\\\\/' ) | ( '->' ) | ( '<->' ) | ( '|' ) );
    public final void rule__Op__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:260:1: ( ( '/\\\\' ) | ( '\\\\/' ) | ( '->' ) | ( '<->' ) | ( '|' ) )
            int alt3=5;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt3=1;
                }
                break;
            case 12:
                {
                alt3=2;
                }
                break;
            case 13:
                {
                alt3=3;
                }
                break;
            case 14:
                {
                alt3=4;
                }
                break;
            case 15:
                {
                alt3=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalFormula.g:261:2: ( '/\\\\' )
                    {
                    // InternalFormula.g:261:2: ( '/\\\\' )
                    // InternalFormula.g:262:3: '/\\\\'
                    {
                     before(grammarAccess.getOpAccess().getSolidusReverseSolidusKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getSolidusReverseSolidusKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:267:2: ( '\\\\/' )
                    {
                    // InternalFormula.g:267:2: ( '\\\\/' )
                    // InternalFormula.g:268:3: '\\\\/'
                    {
                     before(grammarAccess.getOpAccess().getReverseSolidusSolidusKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getReverseSolidusSolidusKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFormula.g:273:2: ( '->' )
                    {
                    // InternalFormula.g:273:2: ( '->' )
                    // InternalFormula.g:274:3: '->'
                    {
                     before(grammarAccess.getOpAccess().getHyphenMinusGreaterThanSignKeyword_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getHyphenMinusGreaterThanSignKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFormula.g:279:2: ( '<->' )
                    {
                    // InternalFormula.g:279:2: ( '<->' )
                    // InternalFormula.g:280:3: '<->'
                    {
                     before(grammarAccess.getOpAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalFormula.g:285:2: ( '|' )
                    {
                    // InternalFormula.g:285:2: ( '|' )
                    // InternalFormula.g:286:3: '|'
                    {
                     before(grammarAccess.getOpAccess().getVerticalLineKeyword_4()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getVerticalLineKeyword_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Op__Alternatives"


    // $ANTLR start "rule__Constant__Group_0__0"
    // InternalFormula.g:295:1: rule__Constant__Group_0__0 : rule__Constant__Group_0__0__Impl rule__Constant__Group_0__1 ;
    public final void rule__Constant__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:299:1: ( rule__Constant__Group_0__0__Impl rule__Constant__Group_0__1 )
            // InternalFormula.g:300:2: rule__Constant__Group_0__0__Impl rule__Constant__Group_0__1
            {
            pushFollow(FOLLOW_1);
            rule__Constant__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constant__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__0"


    // $ANTLR start "rule__Constant__Group_0__0__Impl"
    // InternalFormula.g:307:1: rule__Constant__Group_0__0__Impl : ( 'TRUE' ) ;
    public final void rule__Constant__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:311:1: ( ( 'TRUE' ) )
            // InternalFormula.g:312:1: ( 'TRUE' )
            {
            // InternalFormula.g:312:1: ( 'TRUE' )
            // InternalFormula.g:313:2: 'TRUE'
            {
             before(grammarAccess.getConstantAccess().getTRUEKeyword_0_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getConstantAccess().getTRUEKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__0__Impl"


    // $ANTLR start "rule__Constant__Group_0__1"
    // InternalFormula.g:322:1: rule__Constant__Group_0__1 : rule__Constant__Group_0__1__Impl ;
    public final void rule__Constant__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:326:1: ( rule__Constant__Group_0__1__Impl )
            // InternalFormula.g:327:2: rule__Constant__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__1"


    // $ANTLR start "rule__Constant__Group_0__1__Impl"
    // InternalFormula.g:333:1: rule__Constant__Group_0__1__Impl : ( () ) ;
    public final void rule__Constant__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:337:1: ( ( () ) )
            // InternalFormula.g:338:1: ( () )
            {
            // InternalFormula.g:338:1: ( () )
            // InternalFormula.g:339:2: ()
            {
             before(grammarAccess.getConstantAccess().getConstantAction_0_1()); 
            // InternalFormula.g:340:2: ()
            // InternalFormula.g:340:3: 
            {
            }

             after(grammarAccess.getConstantAccess().getConstantAction_0_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__1__Impl"


    // $ANTLR start "rule__Constant__Group_1__0"
    // InternalFormula.g:349:1: rule__Constant__Group_1__0 : rule__Constant__Group_1__0__Impl rule__Constant__Group_1__1 ;
    public final void rule__Constant__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:353:1: ( rule__Constant__Group_1__0__Impl rule__Constant__Group_1__1 )
            // InternalFormula.g:354:2: rule__Constant__Group_1__0__Impl rule__Constant__Group_1__1
            {
            pushFollow(FOLLOW_1);
            rule__Constant__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constant__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__0"


    // $ANTLR start "rule__Constant__Group_1__0__Impl"
    // InternalFormula.g:361:1: rule__Constant__Group_1__0__Impl : ( 'FALSE' ) ;
    public final void rule__Constant__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:365:1: ( ( 'FALSE' ) )
            // InternalFormula.g:366:1: ( 'FALSE' )
            {
            // InternalFormula.g:366:1: ( 'FALSE' )
            // InternalFormula.g:367:2: 'FALSE'
            {
             before(grammarAccess.getConstantAccess().getFALSEKeyword_1_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getConstantAccess().getFALSEKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__0__Impl"


    // $ANTLR start "rule__Constant__Group_1__1"
    // InternalFormula.g:376:1: rule__Constant__Group_1__1 : rule__Constant__Group_1__1__Impl ;
    public final void rule__Constant__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:380:1: ( rule__Constant__Group_1__1__Impl )
            // InternalFormula.g:381:2: rule__Constant__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__1"


    // $ANTLR start "rule__Constant__Group_1__1__Impl"
    // InternalFormula.g:387:1: rule__Constant__Group_1__1__Impl : ( () ) ;
    public final void rule__Constant__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:391:1: ( ( () ) )
            // InternalFormula.g:392:1: ( () )
            {
            // InternalFormula.g:392:1: ( () )
            // InternalFormula.g:393:2: ()
            {
             before(grammarAccess.getConstantAccess().getConstantAction_1_1()); 
            // InternalFormula.g:394:2: ()
            // InternalFormula.g:394:3: 
            {
            }

             after(grammarAccess.getConstantAccess().getConstantAction_1_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__1__Impl"


    // $ANTLR start "rule__Neg__Group__0"
    // InternalFormula.g:403:1: rule__Neg__Group__0 : rule__Neg__Group__0__Impl rule__Neg__Group__1 ;
    public final void rule__Neg__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:407:1: ( rule__Neg__Group__0__Impl rule__Neg__Group__1 )
            // InternalFormula.g:408:2: rule__Neg__Group__0__Impl rule__Neg__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Neg__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Neg__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__0"


    // $ANTLR start "rule__Neg__Group__0__Impl"
    // InternalFormula.g:415:1: rule__Neg__Group__0__Impl : ( '~' ) ;
    public final void rule__Neg__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:419:1: ( ( '~' ) )
            // InternalFormula.g:420:1: ( '~' )
            {
            // InternalFormula.g:420:1: ( '~' )
            // InternalFormula.g:421:2: '~'
            {
             before(grammarAccess.getNegAccess().getTildeKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getNegAccess().getTildeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__0__Impl"


    // $ANTLR start "rule__Neg__Group__1"
    // InternalFormula.g:430:1: rule__Neg__Group__1 : rule__Neg__Group__1__Impl ;
    public final void rule__Neg__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:434:1: ( rule__Neg__Group__1__Impl )
            // InternalFormula.g:435:2: rule__Neg__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Neg__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__1"


    // $ANTLR start "rule__Neg__Group__1__Impl"
    // InternalFormula.g:441:1: rule__Neg__Group__1__Impl : ( ( rule__Neg__SubAssignment_1 ) ) ;
    public final void rule__Neg__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:445:1: ( ( ( rule__Neg__SubAssignment_1 ) ) )
            // InternalFormula.g:446:1: ( ( rule__Neg__SubAssignment_1 ) )
            {
            // InternalFormula.g:446:1: ( ( rule__Neg__SubAssignment_1 ) )
            // InternalFormula.g:447:2: ( rule__Neg__SubAssignment_1 )
            {
             before(grammarAccess.getNegAccess().getSubAssignment_1()); 
            // InternalFormula.g:448:2: ( rule__Neg__SubAssignment_1 )
            // InternalFormula.g:448:3: rule__Neg__SubAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Neg__SubAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getNegAccess().getSubAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__1__Impl"


    // $ANTLR start "rule__Binop__Group__0"
    // InternalFormula.g:457:1: rule__Binop__Group__0 : rule__Binop__Group__0__Impl rule__Binop__Group__1 ;
    public final void rule__Binop__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:461:1: ( rule__Binop__Group__0__Impl rule__Binop__Group__1 )
            // InternalFormula.g:462:2: rule__Binop__Group__0__Impl rule__Binop__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Binop__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__0"


    // $ANTLR start "rule__Binop__Group__0__Impl"
    // InternalFormula.g:469:1: rule__Binop__Group__0__Impl : ( '(' ) ;
    public final void rule__Binop__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:473:1: ( ( '(' ) )
            // InternalFormula.g:474:1: ( '(' )
            {
            // InternalFormula.g:474:1: ( '(' )
            // InternalFormula.g:475:2: '('
            {
             before(grammarAccess.getBinopAccess().getLeftParenthesisKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getBinopAccess().getLeftParenthesisKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__0__Impl"


    // $ANTLR start "rule__Binop__Group__1"
    // InternalFormula.g:484:1: rule__Binop__Group__1 : rule__Binop__Group__1__Impl rule__Binop__Group__2 ;
    public final void rule__Binop__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:488:1: ( rule__Binop__Group__1__Impl rule__Binop__Group__2 )
            // InternalFormula.g:489:2: rule__Binop__Group__1__Impl rule__Binop__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Binop__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__1"


    // $ANTLR start "rule__Binop__Group__1__Impl"
    // InternalFormula.g:496:1: rule__Binop__Group__1__Impl : ( ruleFormula ) ;
    public final void rule__Binop__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:500:1: ( ( ruleFormula ) )
            // InternalFormula.g:501:1: ( ruleFormula )
            {
            // InternalFormula.g:501:1: ( ruleFormula )
            // InternalFormula.g:502:2: ruleFormula
            {
             before(grammarAccess.getBinopAccess().getFormulaParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getBinopAccess().getFormulaParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__1__Impl"


    // $ANTLR start "rule__Binop__Group__2"
    // InternalFormula.g:511:1: rule__Binop__Group__2 : rule__Binop__Group__2__Impl rule__Binop__Group__3 ;
    public final void rule__Binop__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:515:1: ( rule__Binop__Group__2__Impl rule__Binop__Group__3 )
            // InternalFormula.g:516:2: rule__Binop__Group__2__Impl rule__Binop__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Binop__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__2"


    // $ANTLR start "rule__Binop__Group__2__Impl"
    // InternalFormula.g:523:1: rule__Binop__Group__2__Impl : ( () ) ;
    public final void rule__Binop__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:527:1: ( ( () ) )
            // InternalFormula.g:528:1: ( () )
            {
            // InternalFormula.g:528:1: ( () )
            // InternalFormula.g:529:2: ()
            {
             before(grammarAccess.getBinopAccess().getBinopLeftAction_2()); 
            // InternalFormula.g:530:2: ()
            // InternalFormula.g:530:3: 
            {
            }

             after(grammarAccess.getBinopAccess().getBinopLeftAction_2()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__2__Impl"


    // $ANTLR start "rule__Binop__Group__3"
    // InternalFormula.g:538:1: rule__Binop__Group__3 : rule__Binop__Group__3__Impl rule__Binop__Group__4 ;
    public final void rule__Binop__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:542:1: ( rule__Binop__Group__3__Impl rule__Binop__Group__4 )
            // InternalFormula.g:543:2: rule__Binop__Group__3__Impl rule__Binop__Group__4
            {
            pushFollow(FOLLOW_3);
            rule__Binop__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__3"


    // $ANTLR start "rule__Binop__Group__3__Impl"
    // InternalFormula.g:550:1: rule__Binop__Group__3__Impl : ( ( rule__Binop__OpAssignment_3 ) ) ;
    public final void rule__Binop__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:554:1: ( ( ( rule__Binop__OpAssignment_3 ) ) )
            // InternalFormula.g:555:1: ( ( rule__Binop__OpAssignment_3 ) )
            {
            // InternalFormula.g:555:1: ( ( rule__Binop__OpAssignment_3 ) )
            // InternalFormula.g:556:2: ( rule__Binop__OpAssignment_3 )
            {
             before(grammarAccess.getBinopAccess().getOpAssignment_3()); 
            // InternalFormula.g:557:2: ( rule__Binop__OpAssignment_3 )
            // InternalFormula.g:557:3: rule__Binop__OpAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Binop__OpAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getBinopAccess().getOpAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__3__Impl"


    // $ANTLR start "rule__Binop__Group__4"
    // InternalFormula.g:565:1: rule__Binop__Group__4 : rule__Binop__Group__4__Impl rule__Binop__Group__5 ;
    public final void rule__Binop__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:569:1: ( rule__Binop__Group__4__Impl rule__Binop__Group__5 )
            // InternalFormula.g:570:2: rule__Binop__Group__4__Impl rule__Binop__Group__5
            {
            pushFollow(FOLLOW_5);
            rule__Binop__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__4"


    // $ANTLR start "rule__Binop__Group__4__Impl"
    // InternalFormula.g:577:1: rule__Binop__Group__4__Impl : ( ( rule__Binop__RightAssignment_4 ) ) ;
    public final void rule__Binop__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:581:1: ( ( ( rule__Binop__RightAssignment_4 ) ) )
            // InternalFormula.g:582:1: ( ( rule__Binop__RightAssignment_4 ) )
            {
            // InternalFormula.g:582:1: ( ( rule__Binop__RightAssignment_4 ) )
            // InternalFormula.g:583:2: ( rule__Binop__RightAssignment_4 )
            {
             before(grammarAccess.getBinopAccess().getRightAssignment_4()); 
            // InternalFormula.g:584:2: ( rule__Binop__RightAssignment_4 )
            // InternalFormula.g:584:3: rule__Binop__RightAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Binop__RightAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getBinopAccess().getRightAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__4__Impl"


    // $ANTLR start "rule__Binop__Group__5"
    // InternalFormula.g:592:1: rule__Binop__Group__5 : rule__Binop__Group__5__Impl ;
    public final void rule__Binop__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:596:1: ( rule__Binop__Group__5__Impl )
            // InternalFormula.g:597:2: rule__Binop__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Binop__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__5"


    // $ANTLR start "rule__Binop__Group__5__Impl"
    // InternalFormula.g:603:1: rule__Binop__Group__5__Impl : ( ')' ) ;
    public final void rule__Binop__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:607:1: ( ( ')' ) )
            // InternalFormula.g:608:1: ( ')' )
            {
            // InternalFormula.g:608:1: ( ')' )
            // InternalFormula.g:609:2: ')'
            {
             before(grammarAccess.getBinopAccess().getRightParenthesisKeyword_5()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getBinopAccess().getRightParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__5__Impl"


    // $ANTLR start "rule__Variable__NameAssignment"
    // InternalFormula.g:619:1: rule__Variable__NameAssignment : ( RULE_ID ) ;
    public final void rule__Variable__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:623:1: ( ( RULE_ID ) )
            // InternalFormula.g:624:2: ( RULE_ID )
            {
            // InternalFormula.g:624:2: ( RULE_ID )
            // InternalFormula.g:625:3: RULE_ID
            {
             before(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__NameAssignment"


    // $ANTLR start "rule__Neg__SubAssignment_1"
    // InternalFormula.g:634:1: rule__Neg__SubAssignment_1 : ( ruleFormula ) ;
    public final void rule__Neg__SubAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:638:1: ( ( ruleFormula ) )
            // InternalFormula.g:639:2: ( ruleFormula )
            {
            // InternalFormula.g:639:2: ( ruleFormula )
            // InternalFormula.g:640:3: ruleFormula
            {
             before(grammarAccess.getNegAccess().getSubFormulaParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getNegAccess().getSubFormulaParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__SubAssignment_1"


    // $ANTLR start "rule__Binop__OpAssignment_3"
    // InternalFormula.g:649:1: rule__Binop__OpAssignment_3 : ( ruleOp ) ;
    public final void rule__Binop__OpAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:653:1: ( ( ruleOp ) )
            // InternalFormula.g:654:2: ( ruleOp )
            {
            // InternalFormula.g:654:2: ( ruleOp )
            // InternalFormula.g:655:3: ruleOp
            {
             before(grammarAccess.getBinopAccess().getOpOpParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOp();

            state._fsp--;

             after(grammarAccess.getBinopAccess().getOpOpParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__OpAssignment_3"


    // $ANTLR start "rule__Binop__RightAssignment_4"
    // InternalFormula.g:664:1: rule__Binop__RightAssignment_4 : ( ruleFormula ) ;
    public final void rule__Binop__RightAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:668:1: ( ( ruleFormula ) )
            // InternalFormula.g:669:2: ( ruleFormula )
            {
            // InternalFormula.g:669:2: ( ruleFormula )
            // InternalFormula.g:670:3: ruleFormula
            {
             before(grammarAccess.getBinopAccess().getRightFormulaParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getBinopAccess().getRightFormulaParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__RightAssignment_4"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000000F0010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000000000000F800L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000100000L});

}