package org.xtext.dsl2018.milestone2.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.dsl2018.milestone2.services.FormulaGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFormulaParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'/\\\\'", "'\\\\/'", "'->'", "'<->'", "'|'", "'SolveCNF'", "'.'", "'TRUE'", "'FALSE'", "'~'", "'('", "')'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalFormulaParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFormulaParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFormulaParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFormula.g"; }


    	private FormulaGrammarAccess grammarAccess;

    	public void setGrammarAccess(FormulaGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleSolve"
    // InternalFormula.g:53:1: entryRuleSolve : ruleSolve EOF ;
    public final void entryRuleSolve() throws RecognitionException {
        try {
            // InternalFormula.g:54:1: ( ruleSolve EOF )
            // InternalFormula.g:55:1: ruleSolve EOF
            {
             before(grammarAccess.getSolveRule()); 
            pushFollow(FOLLOW_1);
            ruleSolve();

            state._fsp--;

             after(grammarAccess.getSolveRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSolve"


    // $ANTLR start "ruleSolve"
    // InternalFormula.g:62:1: ruleSolve : ( ( rule__Solve__Group__0 ) ) ;
    public final void ruleSolve() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:66:2: ( ( ( rule__Solve__Group__0 ) ) )
            // InternalFormula.g:67:2: ( ( rule__Solve__Group__0 ) )
            {
            // InternalFormula.g:67:2: ( ( rule__Solve__Group__0 ) )
            // InternalFormula.g:68:3: ( rule__Solve__Group__0 )
            {
             before(grammarAccess.getSolveAccess().getGroup()); 
            // InternalFormula.g:69:3: ( rule__Solve__Group__0 )
            // InternalFormula.g:69:4: rule__Solve__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Solve__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSolveAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSolve"


    // $ANTLR start "entryRuleFormula"
    // InternalFormula.g:78:1: entryRuleFormula : ruleFormula EOF ;
    public final void entryRuleFormula() throws RecognitionException {
        try {
            // InternalFormula.g:79:1: ( ruleFormula EOF )
            // InternalFormula.g:80:1: ruleFormula EOF
            {
             before(grammarAccess.getFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // InternalFormula.g:87:1: ruleFormula : ( ( rule__Formula__Alternatives ) ) ;
    public final void ruleFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:91:2: ( ( ( rule__Formula__Alternatives ) ) )
            // InternalFormula.g:92:2: ( ( rule__Formula__Alternatives ) )
            {
            // InternalFormula.g:92:2: ( ( rule__Formula__Alternatives ) )
            // InternalFormula.g:93:3: ( rule__Formula__Alternatives )
            {
             before(grammarAccess.getFormulaAccess().getAlternatives()); 
            // InternalFormula.g:94:3: ( rule__Formula__Alternatives )
            // InternalFormula.g:94:4: rule__Formula__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Formula__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFormulaAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleConstant"
    // InternalFormula.g:103:1: entryRuleConstant : ruleConstant EOF ;
    public final void entryRuleConstant() throws RecognitionException {
        try {
            // InternalFormula.g:104:1: ( ruleConstant EOF )
            // InternalFormula.g:105:1: ruleConstant EOF
            {
             before(grammarAccess.getConstantRule()); 
            pushFollow(FOLLOW_1);
            ruleConstant();

            state._fsp--;

             after(grammarAccess.getConstantRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // InternalFormula.g:112:1: ruleConstant : ( ( rule__Constant__Alternatives ) ) ;
    public final void ruleConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:116:2: ( ( ( rule__Constant__Alternatives ) ) )
            // InternalFormula.g:117:2: ( ( rule__Constant__Alternatives ) )
            {
            // InternalFormula.g:117:2: ( ( rule__Constant__Alternatives ) )
            // InternalFormula.g:118:3: ( rule__Constant__Alternatives )
            {
             before(grammarAccess.getConstantAccess().getAlternatives()); 
            // InternalFormula.g:119:3: ( rule__Constant__Alternatives )
            // InternalFormula.g:119:4: rule__Constant__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getConstantAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleVariable"
    // InternalFormula.g:128:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // InternalFormula.g:129:1: ( ruleVariable EOF )
            // InternalFormula.g:130:1: ruleVariable EOF
            {
             before(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalFormula.g:137:1: ruleVariable : ( ( rule__Variable__NameAssignment ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:141:2: ( ( ( rule__Variable__NameAssignment ) ) )
            // InternalFormula.g:142:2: ( ( rule__Variable__NameAssignment ) )
            {
            // InternalFormula.g:142:2: ( ( rule__Variable__NameAssignment ) )
            // InternalFormula.g:143:3: ( rule__Variable__NameAssignment )
            {
             before(grammarAccess.getVariableAccess().getNameAssignment()); 
            // InternalFormula.g:144:3: ( rule__Variable__NameAssignment )
            // InternalFormula.g:144:4: rule__Variable__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Variable__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleNeg"
    // InternalFormula.g:153:1: entryRuleNeg : ruleNeg EOF ;
    public final void entryRuleNeg() throws RecognitionException {
        try {
            // InternalFormula.g:154:1: ( ruleNeg EOF )
            // InternalFormula.g:155:1: ruleNeg EOF
            {
             before(grammarAccess.getNegRule()); 
            pushFollow(FOLLOW_1);
            ruleNeg();

            state._fsp--;

             after(grammarAccess.getNegRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNeg"


    // $ANTLR start "ruleNeg"
    // InternalFormula.g:162:1: ruleNeg : ( ( rule__Neg__Group__0 ) ) ;
    public final void ruleNeg() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:166:2: ( ( ( rule__Neg__Group__0 ) ) )
            // InternalFormula.g:167:2: ( ( rule__Neg__Group__0 ) )
            {
            // InternalFormula.g:167:2: ( ( rule__Neg__Group__0 ) )
            // InternalFormula.g:168:3: ( rule__Neg__Group__0 )
            {
             before(grammarAccess.getNegAccess().getGroup()); 
            // InternalFormula.g:169:3: ( rule__Neg__Group__0 )
            // InternalFormula.g:169:4: rule__Neg__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Neg__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNegAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNeg"


    // $ANTLR start "entryRuleBinop"
    // InternalFormula.g:178:1: entryRuleBinop : ruleBinop EOF ;
    public final void entryRuleBinop() throws RecognitionException {
        try {
            // InternalFormula.g:179:1: ( ruleBinop EOF )
            // InternalFormula.g:180:1: ruleBinop EOF
            {
             before(grammarAccess.getBinopRule()); 
            pushFollow(FOLLOW_1);
            ruleBinop();

            state._fsp--;

             after(grammarAccess.getBinopRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinop"


    // $ANTLR start "ruleBinop"
    // InternalFormula.g:187:1: ruleBinop : ( ( rule__Binop__Group__0 ) ) ;
    public final void ruleBinop() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:191:2: ( ( ( rule__Binop__Group__0 ) ) )
            // InternalFormula.g:192:2: ( ( rule__Binop__Group__0 ) )
            {
            // InternalFormula.g:192:2: ( ( rule__Binop__Group__0 ) )
            // InternalFormula.g:193:3: ( rule__Binop__Group__0 )
            {
             before(grammarAccess.getBinopAccess().getGroup()); 
            // InternalFormula.g:194:3: ( rule__Binop__Group__0 )
            // InternalFormula.g:194:4: rule__Binop__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Binop__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBinopAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinop"


    // $ANTLR start "entryRuleOp"
    // InternalFormula.g:203:1: entryRuleOp : ruleOp EOF ;
    public final void entryRuleOp() throws RecognitionException {
        try {
            // InternalFormula.g:204:1: ( ruleOp EOF )
            // InternalFormula.g:205:1: ruleOp EOF
            {
             before(grammarAccess.getOpRule()); 
            pushFollow(FOLLOW_1);
            ruleOp();

            state._fsp--;

             after(grammarAccess.getOpRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOp"


    // $ANTLR start "ruleOp"
    // InternalFormula.g:212:1: ruleOp : ( ( rule__Op__Alternatives ) ) ;
    public final void ruleOp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:216:2: ( ( ( rule__Op__Alternatives ) ) )
            // InternalFormula.g:217:2: ( ( rule__Op__Alternatives ) )
            {
            // InternalFormula.g:217:2: ( ( rule__Op__Alternatives ) )
            // InternalFormula.g:218:3: ( rule__Op__Alternatives )
            {
             before(grammarAccess.getOpAccess().getAlternatives()); 
            // InternalFormula.g:219:3: ( rule__Op__Alternatives )
            // InternalFormula.g:219:4: rule__Op__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Op__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOpAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOp"


    // $ANTLR start "rule__Formula__Alternatives"
    // InternalFormula.g:227:1: rule__Formula__Alternatives : ( ( ruleConstant ) | ( ruleVariable ) | ( ruleNeg ) | ( ruleBinop ) );
    public final void rule__Formula__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:231:1: ( ( ruleConstant ) | ( ruleVariable ) | ( ruleNeg ) | ( ruleBinop ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 18:
            case 19:
                {
                alt1=1;
                }
                break;
            case RULE_ID:
                {
                alt1=2;
                }
                break;
            case 20:
                {
                alt1=3;
                }
                break;
            case 21:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalFormula.g:232:2: ( ruleConstant )
                    {
                    // InternalFormula.g:232:2: ( ruleConstant )
                    // InternalFormula.g:233:3: ruleConstant
                    {
                     before(grammarAccess.getFormulaAccess().getConstantParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleConstant();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getConstantParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:238:2: ( ruleVariable )
                    {
                    // InternalFormula.g:238:2: ( ruleVariable )
                    // InternalFormula.g:239:3: ruleVariable
                    {
                     before(grammarAccess.getFormulaAccess().getVariableParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleVariable();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getVariableParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFormula.g:244:2: ( ruleNeg )
                    {
                    // InternalFormula.g:244:2: ( ruleNeg )
                    // InternalFormula.g:245:3: ruleNeg
                    {
                     before(grammarAccess.getFormulaAccess().getNegParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleNeg();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getNegParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFormula.g:250:2: ( ruleBinop )
                    {
                    // InternalFormula.g:250:2: ( ruleBinop )
                    // InternalFormula.g:251:3: ruleBinop
                    {
                     before(grammarAccess.getFormulaAccess().getBinopParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleBinop();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getBinopParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Alternatives"


    // $ANTLR start "rule__Constant__Alternatives"
    // InternalFormula.g:260:1: rule__Constant__Alternatives : ( ( ( rule__Constant__Group_0__0 ) ) | ( ( rule__Constant__Group_1__0 ) ) );
    public final void rule__Constant__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:264:1: ( ( ( rule__Constant__Group_0__0 ) ) | ( ( rule__Constant__Group_1__0 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==18) ) {
                alt2=1;
            }
            else if ( (LA2_0==19) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalFormula.g:265:2: ( ( rule__Constant__Group_0__0 ) )
                    {
                    // InternalFormula.g:265:2: ( ( rule__Constant__Group_0__0 ) )
                    // InternalFormula.g:266:3: ( rule__Constant__Group_0__0 )
                    {
                     before(grammarAccess.getConstantAccess().getGroup_0()); 
                    // InternalFormula.g:267:3: ( rule__Constant__Group_0__0 )
                    // InternalFormula.g:267:4: rule__Constant__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Constant__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstantAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:271:2: ( ( rule__Constant__Group_1__0 ) )
                    {
                    // InternalFormula.g:271:2: ( ( rule__Constant__Group_1__0 ) )
                    // InternalFormula.g:272:3: ( rule__Constant__Group_1__0 )
                    {
                     before(grammarAccess.getConstantAccess().getGroup_1()); 
                    // InternalFormula.g:273:3: ( rule__Constant__Group_1__0 )
                    // InternalFormula.g:273:4: rule__Constant__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Constant__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstantAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Alternatives"


    // $ANTLR start "rule__Op__Alternatives"
    // InternalFormula.g:281:1: rule__Op__Alternatives : ( ( '/\\\\' ) | ( '\\\\/' ) | ( '->' ) | ( '<->' ) | ( '|' ) );
    public final void rule__Op__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:285:1: ( ( '/\\\\' ) | ( '\\\\/' ) | ( '->' ) | ( '<->' ) | ( '|' ) )
            int alt3=5;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt3=1;
                }
                break;
            case 12:
                {
                alt3=2;
                }
                break;
            case 13:
                {
                alt3=3;
                }
                break;
            case 14:
                {
                alt3=4;
                }
                break;
            case 15:
                {
                alt3=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalFormula.g:286:2: ( '/\\\\' )
                    {
                    // InternalFormula.g:286:2: ( '/\\\\' )
                    // InternalFormula.g:287:3: '/\\\\'
                    {
                     before(grammarAccess.getOpAccess().getSolidusReverseSolidusKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getSolidusReverseSolidusKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFormula.g:292:2: ( '\\\\/' )
                    {
                    // InternalFormula.g:292:2: ( '\\\\/' )
                    // InternalFormula.g:293:3: '\\\\/'
                    {
                     before(grammarAccess.getOpAccess().getReverseSolidusSolidusKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getReverseSolidusSolidusKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFormula.g:298:2: ( '->' )
                    {
                    // InternalFormula.g:298:2: ( '->' )
                    // InternalFormula.g:299:3: '->'
                    {
                     before(grammarAccess.getOpAccess().getHyphenMinusGreaterThanSignKeyword_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getHyphenMinusGreaterThanSignKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFormula.g:304:2: ( '<->' )
                    {
                    // InternalFormula.g:304:2: ( '<->' )
                    // InternalFormula.g:305:3: '<->'
                    {
                     before(grammarAccess.getOpAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalFormula.g:310:2: ( '|' )
                    {
                    // InternalFormula.g:310:2: ( '|' )
                    // InternalFormula.g:311:3: '|'
                    {
                     before(grammarAccess.getOpAccess().getVerticalLineKeyword_4()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getVerticalLineKeyword_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Op__Alternatives"


    // $ANTLR start "rule__Solve__Group__0"
    // InternalFormula.g:320:1: rule__Solve__Group__0 : rule__Solve__Group__0__Impl rule__Solve__Group__1 ;
    public final void rule__Solve__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:324:1: ( rule__Solve__Group__0__Impl rule__Solve__Group__1 )
            // InternalFormula.g:325:2: rule__Solve__Group__0__Impl rule__Solve__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Solve__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solve__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__0"


    // $ANTLR start "rule__Solve__Group__0__Impl"
    // InternalFormula.g:332:1: rule__Solve__Group__0__Impl : ( 'SolveCNF' ) ;
    public final void rule__Solve__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:336:1: ( ( 'SolveCNF' ) )
            // InternalFormula.g:337:1: ( 'SolveCNF' )
            {
            // InternalFormula.g:337:1: ( 'SolveCNF' )
            // InternalFormula.g:338:2: 'SolveCNF'
            {
             before(grammarAccess.getSolveAccess().getSolveCNFKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getSolveAccess().getSolveCNFKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__0__Impl"


    // $ANTLR start "rule__Solve__Group__1"
    // InternalFormula.g:347:1: rule__Solve__Group__1 : rule__Solve__Group__1__Impl rule__Solve__Group__2 ;
    public final void rule__Solve__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:351:1: ( rule__Solve__Group__1__Impl rule__Solve__Group__2 )
            // InternalFormula.g:352:2: rule__Solve__Group__1__Impl rule__Solve__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Solve__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solve__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__1"


    // $ANTLR start "rule__Solve__Group__1__Impl"
    // InternalFormula.g:359:1: rule__Solve__Group__1__Impl : ( ( rule__Solve__FormulaAssignment_1 ) ) ;
    public final void rule__Solve__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:363:1: ( ( ( rule__Solve__FormulaAssignment_1 ) ) )
            // InternalFormula.g:364:1: ( ( rule__Solve__FormulaAssignment_1 ) )
            {
            // InternalFormula.g:364:1: ( ( rule__Solve__FormulaAssignment_1 ) )
            // InternalFormula.g:365:2: ( rule__Solve__FormulaAssignment_1 )
            {
             before(grammarAccess.getSolveAccess().getFormulaAssignment_1()); 
            // InternalFormula.g:366:2: ( rule__Solve__FormulaAssignment_1 )
            // InternalFormula.g:366:3: rule__Solve__FormulaAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Solve__FormulaAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSolveAccess().getFormulaAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__1__Impl"


    // $ANTLR start "rule__Solve__Group__2"
    // InternalFormula.g:374:1: rule__Solve__Group__2 : rule__Solve__Group__2__Impl ;
    public final void rule__Solve__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:378:1: ( rule__Solve__Group__2__Impl )
            // InternalFormula.g:379:2: rule__Solve__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Solve__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__2"


    // $ANTLR start "rule__Solve__Group__2__Impl"
    // InternalFormula.g:385:1: rule__Solve__Group__2__Impl : ( '.' ) ;
    public final void rule__Solve__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:389:1: ( ( '.' ) )
            // InternalFormula.g:390:1: ( '.' )
            {
            // InternalFormula.g:390:1: ( '.' )
            // InternalFormula.g:391:2: '.'
            {
             before(grammarAccess.getSolveAccess().getFullStopKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSolveAccess().getFullStopKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__Group__2__Impl"


    // $ANTLR start "rule__Constant__Group_0__0"
    // InternalFormula.g:401:1: rule__Constant__Group_0__0 : rule__Constant__Group_0__0__Impl rule__Constant__Group_0__1 ;
    public final void rule__Constant__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:405:1: ( rule__Constant__Group_0__0__Impl rule__Constant__Group_0__1 )
            // InternalFormula.g:406:2: rule__Constant__Group_0__0__Impl rule__Constant__Group_0__1
            {
            pushFollow(FOLLOW_1);
            rule__Constant__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constant__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__0"


    // $ANTLR start "rule__Constant__Group_0__0__Impl"
    // InternalFormula.g:413:1: rule__Constant__Group_0__0__Impl : ( 'TRUE' ) ;
    public final void rule__Constant__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:417:1: ( ( 'TRUE' ) )
            // InternalFormula.g:418:1: ( 'TRUE' )
            {
            // InternalFormula.g:418:1: ( 'TRUE' )
            // InternalFormula.g:419:2: 'TRUE'
            {
             before(grammarAccess.getConstantAccess().getTRUEKeyword_0_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getConstantAccess().getTRUEKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__0__Impl"


    // $ANTLR start "rule__Constant__Group_0__1"
    // InternalFormula.g:428:1: rule__Constant__Group_0__1 : rule__Constant__Group_0__1__Impl ;
    public final void rule__Constant__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:432:1: ( rule__Constant__Group_0__1__Impl )
            // InternalFormula.g:433:2: rule__Constant__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__1"


    // $ANTLR start "rule__Constant__Group_0__1__Impl"
    // InternalFormula.g:439:1: rule__Constant__Group_0__1__Impl : ( () ) ;
    public final void rule__Constant__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:443:1: ( ( () ) )
            // InternalFormula.g:444:1: ( () )
            {
            // InternalFormula.g:444:1: ( () )
            // InternalFormula.g:445:2: ()
            {
             before(grammarAccess.getConstantAccess().getConstantAction_0_1()); 
            // InternalFormula.g:446:2: ()
            // InternalFormula.g:446:3: 
            {
            }

             after(grammarAccess.getConstantAccess().getConstantAction_0_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_0__1__Impl"


    // $ANTLR start "rule__Constant__Group_1__0"
    // InternalFormula.g:455:1: rule__Constant__Group_1__0 : rule__Constant__Group_1__0__Impl rule__Constant__Group_1__1 ;
    public final void rule__Constant__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:459:1: ( rule__Constant__Group_1__0__Impl rule__Constant__Group_1__1 )
            // InternalFormula.g:460:2: rule__Constant__Group_1__0__Impl rule__Constant__Group_1__1
            {
            pushFollow(FOLLOW_1);
            rule__Constant__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constant__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__0"


    // $ANTLR start "rule__Constant__Group_1__0__Impl"
    // InternalFormula.g:467:1: rule__Constant__Group_1__0__Impl : ( 'FALSE' ) ;
    public final void rule__Constant__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:471:1: ( ( 'FALSE' ) )
            // InternalFormula.g:472:1: ( 'FALSE' )
            {
            // InternalFormula.g:472:1: ( 'FALSE' )
            // InternalFormula.g:473:2: 'FALSE'
            {
             before(grammarAccess.getConstantAccess().getFALSEKeyword_1_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getConstantAccess().getFALSEKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__0__Impl"


    // $ANTLR start "rule__Constant__Group_1__1"
    // InternalFormula.g:482:1: rule__Constant__Group_1__1 : rule__Constant__Group_1__1__Impl ;
    public final void rule__Constant__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:486:1: ( rule__Constant__Group_1__1__Impl )
            // InternalFormula.g:487:2: rule__Constant__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constant__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__1"


    // $ANTLR start "rule__Constant__Group_1__1__Impl"
    // InternalFormula.g:493:1: rule__Constant__Group_1__1__Impl : ( () ) ;
    public final void rule__Constant__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:497:1: ( ( () ) )
            // InternalFormula.g:498:1: ( () )
            {
            // InternalFormula.g:498:1: ( () )
            // InternalFormula.g:499:2: ()
            {
             before(grammarAccess.getConstantAccess().getConstantAction_1_1()); 
            // InternalFormula.g:500:2: ()
            // InternalFormula.g:500:3: 
            {
            }

             after(grammarAccess.getConstantAccess().getConstantAction_1_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group_1__1__Impl"


    // $ANTLR start "rule__Neg__Group__0"
    // InternalFormula.g:509:1: rule__Neg__Group__0 : rule__Neg__Group__0__Impl rule__Neg__Group__1 ;
    public final void rule__Neg__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:513:1: ( rule__Neg__Group__0__Impl rule__Neg__Group__1 )
            // InternalFormula.g:514:2: rule__Neg__Group__0__Impl rule__Neg__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Neg__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Neg__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__0"


    // $ANTLR start "rule__Neg__Group__0__Impl"
    // InternalFormula.g:521:1: rule__Neg__Group__0__Impl : ( '~' ) ;
    public final void rule__Neg__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:525:1: ( ( '~' ) )
            // InternalFormula.g:526:1: ( '~' )
            {
            // InternalFormula.g:526:1: ( '~' )
            // InternalFormula.g:527:2: '~'
            {
             before(grammarAccess.getNegAccess().getTildeKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getNegAccess().getTildeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__0__Impl"


    // $ANTLR start "rule__Neg__Group__1"
    // InternalFormula.g:536:1: rule__Neg__Group__1 : rule__Neg__Group__1__Impl ;
    public final void rule__Neg__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:540:1: ( rule__Neg__Group__1__Impl )
            // InternalFormula.g:541:2: rule__Neg__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Neg__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__1"


    // $ANTLR start "rule__Neg__Group__1__Impl"
    // InternalFormula.g:547:1: rule__Neg__Group__1__Impl : ( ( rule__Neg__SubAssignment_1 ) ) ;
    public final void rule__Neg__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:551:1: ( ( ( rule__Neg__SubAssignment_1 ) ) )
            // InternalFormula.g:552:1: ( ( rule__Neg__SubAssignment_1 ) )
            {
            // InternalFormula.g:552:1: ( ( rule__Neg__SubAssignment_1 ) )
            // InternalFormula.g:553:2: ( rule__Neg__SubAssignment_1 )
            {
             before(grammarAccess.getNegAccess().getSubAssignment_1()); 
            // InternalFormula.g:554:2: ( rule__Neg__SubAssignment_1 )
            // InternalFormula.g:554:3: rule__Neg__SubAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Neg__SubAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getNegAccess().getSubAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__1__Impl"


    // $ANTLR start "rule__Binop__Group__0"
    // InternalFormula.g:563:1: rule__Binop__Group__0 : rule__Binop__Group__0__Impl rule__Binop__Group__1 ;
    public final void rule__Binop__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:567:1: ( rule__Binop__Group__0__Impl rule__Binop__Group__1 )
            // InternalFormula.g:568:2: rule__Binop__Group__0__Impl rule__Binop__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Binop__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__0"


    // $ANTLR start "rule__Binop__Group__0__Impl"
    // InternalFormula.g:575:1: rule__Binop__Group__0__Impl : ( '(' ) ;
    public final void rule__Binop__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:579:1: ( ( '(' ) )
            // InternalFormula.g:580:1: ( '(' )
            {
            // InternalFormula.g:580:1: ( '(' )
            // InternalFormula.g:581:2: '('
            {
             before(grammarAccess.getBinopAccess().getLeftParenthesisKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getBinopAccess().getLeftParenthesisKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__0__Impl"


    // $ANTLR start "rule__Binop__Group__1"
    // InternalFormula.g:590:1: rule__Binop__Group__1 : rule__Binop__Group__1__Impl rule__Binop__Group__2 ;
    public final void rule__Binop__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:594:1: ( rule__Binop__Group__1__Impl rule__Binop__Group__2 )
            // InternalFormula.g:595:2: rule__Binop__Group__1__Impl rule__Binop__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Binop__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__1"


    // $ANTLR start "rule__Binop__Group__1__Impl"
    // InternalFormula.g:602:1: rule__Binop__Group__1__Impl : ( ruleFormula ) ;
    public final void rule__Binop__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:606:1: ( ( ruleFormula ) )
            // InternalFormula.g:607:1: ( ruleFormula )
            {
            // InternalFormula.g:607:1: ( ruleFormula )
            // InternalFormula.g:608:2: ruleFormula
            {
             before(grammarAccess.getBinopAccess().getFormulaParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getBinopAccess().getFormulaParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__1__Impl"


    // $ANTLR start "rule__Binop__Group__2"
    // InternalFormula.g:617:1: rule__Binop__Group__2 : rule__Binop__Group__2__Impl rule__Binop__Group__3 ;
    public final void rule__Binop__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:621:1: ( rule__Binop__Group__2__Impl rule__Binop__Group__3 )
            // InternalFormula.g:622:2: rule__Binop__Group__2__Impl rule__Binop__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Binop__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__2"


    // $ANTLR start "rule__Binop__Group__2__Impl"
    // InternalFormula.g:629:1: rule__Binop__Group__2__Impl : ( () ) ;
    public final void rule__Binop__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:633:1: ( ( () ) )
            // InternalFormula.g:634:1: ( () )
            {
            // InternalFormula.g:634:1: ( () )
            // InternalFormula.g:635:2: ()
            {
             before(grammarAccess.getBinopAccess().getBinopLeftAction_2()); 
            // InternalFormula.g:636:2: ()
            // InternalFormula.g:636:3: 
            {
            }

             after(grammarAccess.getBinopAccess().getBinopLeftAction_2()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__2__Impl"


    // $ANTLR start "rule__Binop__Group__3"
    // InternalFormula.g:644:1: rule__Binop__Group__3 : rule__Binop__Group__3__Impl rule__Binop__Group__4 ;
    public final void rule__Binop__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:648:1: ( rule__Binop__Group__3__Impl rule__Binop__Group__4 )
            // InternalFormula.g:649:2: rule__Binop__Group__3__Impl rule__Binop__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__Binop__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__3"


    // $ANTLR start "rule__Binop__Group__3__Impl"
    // InternalFormula.g:656:1: rule__Binop__Group__3__Impl : ( ( rule__Binop__OpAssignment_3 ) ) ;
    public final void rule__Binop__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:660:1: ( ( ( rule__Binop__OpAssignment_3 ) ) )
            // InternalFormula.g:661:1: ( ( rule__Binop__OpAssignment_3 ) )
            {
            // InternalFormula.g:661:1: ( ( rule__Binop__OpAssignment_3 ) )
            // InternalFormula.g:662:2: ( rule__Binop__OpAssignment_3 )
            {
             before(grammarAccess.getBinopAccess().getOpAssignment_3()); 
            // InternalFormula.g:663:2: ( rule__Binop__OpAssignment_3 )
            // InternalFormula.g:663:3: rule__Binop__OpAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Binop__OpAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getBinopAccess().getOpAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__3__Impl"


    // $ANTLR start "rule__Binop__Group__4"
    // InternalFormula.g:671:1: rule__Binop__Group__4 : rule__Binop__Group__4__Impl rule__Binop__Group__5 ;
    public final void rule__Binop__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:675:1: ( rule__Binop__Group__4__Impl rule__Binop__Group__5 )
            // InternalFormula.g:676:2: rule__Binop__Group__4__Impl rule__Binop__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Binop__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binop__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__4"


    // $ANTLR start "rule__Binop__Group__4__Impl"
    // InternalFormula.g:683:1: rule__Binop__Group__4__Impl : ( ( rule__Binop__RightAssignment_4 ) ) ;
    public final void rule__Binop__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:687:1: ( ( ( rule__Binop__RightAssignment_4 ) ) )
            // InternalFormula.g:688:1: ( ( rule__Binop__RightAssignment_4 ) )
            {
            // InternalFormula.g:688:1: ( ( rule__Binop__RightAssignment_4 ) )
            // InternalFormula.g:689:2: ( rule__Binop__RightAssignment_4 )
            {
             before(grammarAccess.getBinopAccess().getRightAssignment_4()); 
            // InternalFormula.g:690:2: ( rule__Binop__RightAssignment_4 )
            // InternalFormula.g:690:3: rule__Binop__RightAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Binop__RightAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getBinopAccess().getRightAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__4__Impl"


    // $ANTLR start "rule__Binop__Group__5"
    // InternalFormula.g:698:1: rule__Binop__Group__5 : rule__Binop__Group__5__Impl ;
    public final void rule__Binop__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:702:1: ( rule__Binop__Group__5__Impl )
            // InternalFormula.g:703:2: rule__Binop__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Binop__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__5"


    // $ANTLR start "rule__Binop__Group__5__Impl"
    // InternalFormula.g:709:1: rule__Binop__Group__5__Impl : ( ')' ) ;
    public final void rule__Binop__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:713:1: ( ( ')' ) )
            // InternalFormula.g:714:1: ( ')' )
            {
            // InternalFormula.g:714:1: ( ')' )
            // InternalFormula.g:715:2: ')'
            {
             before(grammarAccess.getBinopAccess().getRightParenthesisKeyword_5()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getBinopAccess().getRightParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__Group__5__Impl"


    // $ANTLR start "rule__Solve__FormulaAssignment_1"
    // InternalFormula.g:725:1: rule__Solve__FormulaAssignment_1 : ( ruleBinop ) ;
    public final void rule__Solve__FormulaAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:729:1: ( ( ruleBinop ) )
            // InternalFormula.g:730:2: ( ruleBinop )
            {
            // InternalFormula.g:730:2: ( ruleBinop )
            // InternalFormula.g:731:3: ruleBinop
            {
             before(grammarAccess.getSolveAccess().getFormulaBinopParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBinop();

            state._fsp--;

             after(grammarAccess.getSolveAccess().getFormulaBinopParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solve__FormulaAssignment_1"


    // $ANTLR start "rule__Variable__NameAssignment"
    // InternalFormula.g:740:1: rule__Variable__NameAssignment : ( RULE_ID ) ;
    public final void rule__Variable__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:744:1: ( ( RULE_ID ) )
            // InternalFormula.g:745:2: ( RULE_ID )
            {
            // InternalFormula.g:745:2: ( RULE_ID )
            // InternalFormula.g:746:3: RULE_ID
            {
             before(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__NameAssignment"


    // $ANTLR start "rule__Neg__SubAssignment_1"
    // InternalFormula.g:755:1: rule__Neg__SubAssignment_1 : ( ruleFormula ) ;
    public final void rule__Neg__SubAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:759:1: ( ( ruleFormula ) )
            // InternalFormula.g:760:2: ( ruleFormula )
            {
            // InternalFormula.g:760:2: ( ruleFormula )
            // InternalFormula.g:761:3: ruleFormula
            {
             before(grammarAccess.getNegAccess().getSubFormulaParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getNegAccess().getSubFormulaParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__SubAssignment_1"


    // $ANTLR start "rule__Binop__OpAssignment_3"
    // InternalFormula.g:770:1: rule__Binop__OpAssignment_3 : ( ruleOp ) ;
    public final void rule__Binop__OpAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:774:1: ( ( ruleOp ) )
            // InternalFormula.g:775:2: ( ruleOp )
            {
            // InternalFormula.g:775:2: ( ruleOp )
            // InternalFormula.g:776:3: ruleOp
            {
             before(grammarAccess.getBinopAccess().getOpOpParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOp();

            state._fsp--;

             after(grammarAccess.getBinopAccess().getOpOpParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__OpAssignment_3"


    // $ANTLR start "rule__Binop__RightAssignment_4"
    // InternalFormula.g:785:1: rule__Binop__RightAssignment_4 : ( ruleFormula ) ;
    public final void rule__Binop__RightAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFormula.g:789:1: ( ( ruleFormula ) )
            // InternalFormula.g:790:2: ( ruleFormula )
            {
            // InternalFormula.g:790:2: ( ruleFormula )
            // InternalFormula.g:791:3: ruleFormula
            {
             before(grammarAccess.getBinopAccess().getRightFormulaParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getBinopAccess().getRightFormulaParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binop__RightAssignment_4"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000003C0010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000000F800L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000400000L});

}