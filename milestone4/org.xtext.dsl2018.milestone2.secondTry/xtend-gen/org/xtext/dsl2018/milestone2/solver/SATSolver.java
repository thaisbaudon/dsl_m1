package org.xtext.dsl2018.milestone2.solver;

import java.io.ByteArrayInputStream;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

@SuppressWarnings("all")
public class SATSolver {
  public Boolean solveDimacs(final String dimacsFormula) {
    try {
      final ISolver solver = SolverFactory.newDefault();
      solver.setTimeout(3600);
      final DimacsReader reader = new DimacsReader(solver);
      try {
        byte[] _bytes = dimacsFormula.getBytes("US-ASCII");
        ByteArrayInputStream _byteArrayInputStream = new ByteArrayInputStream(_bytes);
        final IProblem problem = reader.parseInstance(_byteArrayInputStream);
        boolean _isSatisfiable = problem.isSatisfiable();
        if (_isSatisfiable) {
          System.out.println("SAT");
          return Boolean.valueOf(true);
        } else {
          System.out.println("UNSAT");
          return Boolean.valueOf(false);
        }
      } catch (final Throwable _t) {
        if (_t instanceof ContradictionException) {
          System.out.println("UNSAT (trivial)");
          return Boolean.valueOf(false);
        } else if (_t instanceof TimeoutException) {
          System.out.println("Timeout :(");
          return Boolean.valueOf(false);
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
