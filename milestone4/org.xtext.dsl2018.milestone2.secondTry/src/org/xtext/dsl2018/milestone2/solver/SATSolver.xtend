package org.xtext.dsl2018.milestone2.solver

import org.sat4j.minisat.SolverFactory
import org.sat4j.reader.DimacsReader
import org.sat4j.specs.ContradictionException
import org.sat4j.specs.TimeoutException
import java.io.ByteArrayInputStream

class SATSolver {
	def Boolean solveDimacs(String dimacsFormula) {
		val solver = SolverFactory.newDefault();
        solver.setTimeout(3600); // 1 hour timeout
        val reader = new DimacsReader(solver);

        try {
            //val problem = reader.parseInstance(dimacsFilePath);
            val problem = reader.parseInstance(new ByteArrayInputStream(dimacsFormula.getBytes("US-ASCII")));
            if (problem.isSatisfiable()) {
            	System.out.println("SAT");
            	return true;
            }
            else {
            	System.out.println("UNSAT");
            	return false;
            }
        } catch (ContradictionException e) {
            System.out.println("UNSAT (trivial)");
            return false;
        } catch (TimeoutException e) {
            System.out.println("Timeout :(");
            return false;    
        }
	}
}