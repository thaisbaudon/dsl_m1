/*
 * generated by Xtext 2.15.0
 */
package org.xtext.dsl2018.milestone2.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import org.xtext.dsl2018.milestone2.formula.Binop
import org.xtext.dsl2018.milestone2.formula.Formula
import org.xtext.dsl2018.milestone2.formula.Variable
import java.util.HashMap
import org.xtext.dsl2018.milestone2.formula.Solve
import org.xtext.dsl2018.milestone2.solver.SATSolver
import org.xtext.dsl2018.milestone2.formula.SolveFile

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class FormulaGenerator extends AbstractGenerator {

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		val solver = new SATSolver;
		if (resource.getContents.filter(SolveFile) !== null) {
			solver.solveDimacs()
		}
		val varMap = resource.mapVariables;
		fsa.generateFile('dimacs.txt',
			'p cnf ' + varMap.size + ' ' + resource.nclauses + '\n'
			+ (resource.allContents.filter(Solve).head.formula as Binop)
				.extractClauses(varMap));
		val solver = new SATSolver;
		solver.solveDimacs('dimacs.txt');
	}
	
	def mapVariables(Resource res)
	{
		val map = newHashMap()
		var nextKey = 1
		
		for (v : res.allContents.filter(Variable).toIterable)
			if (!map.containsKey(v.name))
			{
				map.put(v.name, nextKey)
				nextKey++
			}
		
		return map
	}
	
	/*def nvars(Resource res)
		'''«res.allContents.filter(Variable).size»'''*/
	
	def nclauses(Resource res)
		'''«res.allContents.filter(Binop).filter([op == '/\\']).size + 1»'''
		
		//«FOR c : res.allContents.filter(Binop).toIterable.filter([op == '/\\'])»
	def extractClauses(Binop c, HashMap<String, Integer> varmap)
		'''
			«IF c.left.isAnd»
				«(c.left as Binop).extractClauses(varmap)»
			«ELSE»
				«c.left.extractLiterals(varmap)» 0
			«ENDIF»
			«IF c.right.isAnd»
				«(c.right as Binop).extractClauses(varmap)»
			«ELSE»
				«c.right.extractLiterals(varmap)» 0
			«ENDIF»
		'''
	
	def dispatch isAnd(Binop b)
		{ return (b.op == '/\\'); }
		
	def dispatch isAnd(Formula b)
		{ return false; }
	
	def dispatch extractLiterals(Binop d, HashMap<String, Integer> varmap)
		'''«d.left.extractLiterals(varmap)» «d.right.extractLiterals(varmap)»'''
	
	def dispatch extractLiterals(Formula neg, HashMap<String, Integer> varmap)
		'''-«neg.sub.extractLiterals(varmap)»'''
	
	def dispatch extractLiterals(Variable v, HashMap<String, Integer> varmap)
		//'''«v.name»'''
		'''«varmap.get(v.name)»'''
}
